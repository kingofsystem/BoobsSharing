import socket

from client import Client
from server_folder import server
from storage import Storage
from utils import thread

@thread
def new_connection(con, addr, storage):
    Client(con, addr, storage)


def main():
    mongo_storage = Storage()
    sock = socket.socket()

    server.start_server()

    sock.bind(('0.0.0.0', 3509))
    sock.listen()

    while True:
        con, addr = sock.accept()
        new_connection(con, addr, mongo_storage)


if __name__ == '__main__':
    main()
