from pymongo import MongoClient


class Storage:
    def __init__(self):
        # self.client = MongoClient('192.168.0.18', 27017)
        self.client = MongoClient()  # For localhost connection

        self.boobs_db = self.client.boobs_db
        self.clients = self.boobs_db.clients
        self.captchas = self.boobs_db.captchas
        self.boobs = self.boobs_db.boobs

    def get_client(self, addr):
        return self.clients.find_one({'addr': addr})

    def save_client(self, client):
        founded_client = self.clients.find_one({'addr': client.addr})
        if founded_client is None:
            self.clients.insert_one({'addr': client.addr,
                                     'money': client.money,
                                     'bought_items': client.bought_items})
        else:
            self.clients.update_one(
                {'addr': client.addr},
                {
                    '$set': {
                        'money': client.money,
                        'bought_items': client.bought_items
                    }
                }
            )

    def add_captcha(self, id, path, text):
        self.captchas.insert_one({
            'id': id,
            'path': path,
            'text': text,
        })
        return id

    def get_random_captcha(self):
        return self.captchas.aggregate(
            {'$sample': {'size': 1}}
        )

    def get_captcha(self, id):
        return self.captchas.find_one({'id': id})

    def remove_captcha(self, id):
        self.captchas.delete_one({'id': id})

    def add_boobs(self, id, path, secret):
        self.captchas.insert_one({
            'id': id,
            'path': path,
            'secret': secret,
        })

    def get_boobs(self, id):
        return self.boobs.find_one({'id': id})

    def all_boobs(self):
        return self.boobs.find()
