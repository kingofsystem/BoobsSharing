import http.server


class MyVerySecureHTTPHandler(http.server.SimpleHTTPRequestHandler):
    def __init__(self, request, client_address, server):
        super(MyVerySecureHTTPHandler, self).__init__(request,
                                                      client_address, server)


def start_server():
    Handler = http.server.BaseHTTPRequestHandler
    httpd = http.server.HTTPServer(('', 1488), MyVerySecureHTTPHandler)
    print('serving at port', 1488)
    httpd.serve_forever()
