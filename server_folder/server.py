import http.server

from utils import thread


class MyVerySecureHTTPHandler(http.server.SimpleHTTPRequestHandler):
    def __init__(self, request, client_address, server):
        super(MyVerySecureHTTPHandler, self).__init__(request,
                                                      client_address, server)

    def do_GET(self):
        self.path = '/server_folder' + self.path
        super(MyVerySecureHTTPHandler, self).do_GET()


@thread
def start_server():
    httpd = http.server.HTTPServer(('', 1488), MyVerySecureHTTPHandler)
    print('serving at port', 1488)
    httpd.serve_forever()
