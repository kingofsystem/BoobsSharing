from PIL import Image, ImageDraw, ImageFont
import os
import random


def write_on_boobs(flag="0KLQuNGC0LrQuNC/0YDQsNCy0Y/Rgg==",
                   folder="beauty_encoder/boobs/",
                   path_out="boobs_good/result.png"):
    # import Image, ImageDraw
    # text = "Python Imaging Library in Habr :)"
    # color = (0, 0, 120)
    boobs = os.listdir(folder)
    boob = boobs[random.randint(0, len(boobs) - 1)]
    img = Image.open(folder + boob)
    fnt = ImageFont.truetype('beauty_encoder/comic.ttf', 30)
    imgDrawer = ImageDraw.Draw(img)
    imgDrawer.text((100, 200), flag, font=fnt, fill=(255, 255, 255, 128))
    img.save(path_out)


def create_captcha(path_out="captcha_out/result.png"):
    captcha_text = ''
    for i in range(0, 5):
        captcha_text += chr(random.randint(47, 122))

    img = Image.new('RGB', (200, 100), (255, 255, 255, 0))
    fnt = ImageFont.truetype('beauty_encoder/comic.ttf', 50)
    imgDrawer = ImageDraw.Draw(img)
    imgDrawer.text((0, 0), captcha_text, font=fnt, fill=(0, 0, 0, 0))
    img.save(path_out)
    return captcha_text
