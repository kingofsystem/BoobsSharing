# BoobsSharing
Small service for SiBears school CTF. Very easy to use and deploy. 

For run [install MongoDB](https://docs.mongodb.com/getting-started/shell/tutorial/install-on-linux/).

Dependencies are:
 + PyMongo
 + Pillow
 
 You can use requirements.txt:
`pip install -r requirements.txt`