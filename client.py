import utils
import socket
import os
import beauty_encoder
import uuid

_HELP = """
Welcome. This is a best place, where you can earn money and watch something
beautiful. To earn you need to solve captcha of our service.

Commands:
balance - get current balance
solve_captcha - solve captcha of our service. We will give you a link to our
                server, where it placed
place_solve <id> <solve> - place captcha solve
list_beauties - list all rewards, that you can get
buy_beauty <id> - buy beauty with id
""".strip()


class Client:
    def __init__(self, sock, addr, storage):
        self.sock = sock
        self.addr = addr
        self.storage = storage

        my_info = storage.get_client(addr)
        if my_info is None:
            self.money = 0
            self.bought_items = []
        else:
            self.money = my_info['momey']
            self.bought_items = my_info['bought_items']

        self._loop_running = True
        self._listen_loop()

    def _listen_loop(self):
        self._answer(_HELP)
        while self._loop_running:
            try:
                self._answer('=> ', end='')
                message = utils.recv_until(self.sock)
                self._on_message(message)
                if message == '':
                    self._drop_connection()
            except socket.error as e:
                print(e)
                self._drop_connection()

    def _on_message(self, message):
        try:
            slices = message.split()
            self.__getattribute__(slices[0])(*slices[1:])
            self.storage.save_client(self)
        except AttributeError and TypeError as e:
            self._answer('Wrong message')
            pass

    def _answer(self, message, end='\n'):
        try:
            utils.send(self.sock, message, end)
        except socket.error as e:
            self._drop_connection()

    def _drop_connection(self):
        try:
            self.sock.close()
        except Exception as e:
            pass

        self._loop_running = False

    def balance(self):
        self._answer(f'Your money is {self.money}₴')

    def solve_captcha(self):
        captcha_id = str(uuid.uuid4())
        captcha_text = beauty_encoder.create_captcha(
            path_out=f'./server_folder/captchas/{captcha_id}.png')
        self.storage.add_captcha(captcha_id,
                                 f'./server_folder/captchas/{captcha_id}.png',
                                 captcha_text)
        self._answer(
            f'Your captcha on http://this_host:1488/captchas/{captcha_id}.'
            f'png id is {captcha_id}'
        )

    def place_solve(self, id, solve):
        os.system(f'rm ./server_folder/captcha/{id}.png')
        captcha = self.storage.get_captcha(id)
        if captcha['text'].lower() == solve.lower():
            self._answer('Solve is correct!')
            self.money += 10
        else:
            self._answer('Solve is incorrect!')

    def buy_beauty(self, id):
        beauty = self.storage.get_boobs(id)
        if beauty is not None and self.money >= 10:
            self.money -= 10
            self._answer(
                f'Your beaty is on this url: http://this_host:1488/'
                f'{beauty["path"]}')
            self.bought_items.append(id)
        else:
            self._answer('You cannot buy this. Sorry.')

    def add_beauty(self, beauty):
        new_beauty_id = str(uuid.uuid4())
        beauty_encoder.write_on_boobs(beauty,
                                      path_out=f'./server_folder/beauties/'
                                                f'{new_beauty_id}.png')
        self.storage.add_boobs(new_beauty_id,
                               f'./server_folder/beauties/{new_beauty_id}.png',
                               beauty)

    def list_beauties(self):
        for beauty in self.storage.all_boobs():
            self._answer(f'Beauty id: {beauty[id]}')
